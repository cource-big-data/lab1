from random import randrange
import calendar;
import time;

n = 5000000

f = open('/input/readme.txt', 'w')

ts = calendar.timegm(time.gmtime()) * 1000
for _ in range(n):
    num = []
    num.append(str(randrange(1,3)))
    ts = ts + randrange(1,3600000)
    num.append(str(ts))
    num.append(str(randrange(1,1000)))
    result = ', '.join(num)
    f.write(result)
    f.write('\n')

f.close()

