FROM maven:3.6.0-jdk-11-slim AS build
COPY src /home/app/src
COPY pom.xml /home/app
RUN mvn -f /home/app/pom.xml --batch-mode clean package


FROM hadoop-base:latest

COPY --from=build /home/app/target/Labs1-1.0-SNAPSHOT-jar-with-dependencies.jar /opt/hadoop/applications/Labs1.jar

ENV JAR_FILEPATH="/opt/hadoop/applications/Labs1.jar"
ENV CLASS_TO_RUN="Main"
ENV PARAMS="/input /output"

ADD run.sh /run.sh
RUN chmod a+x /run.sh

CMD ["/run.sh"]