package ru.course;

import lombok.extern.log4j.Log4j;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.input.KeyValueTextInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.fs.Path;
import ru.course.data.MetricsKeyOutWritable;
import ru.course.format.MetricsCSVOutputFormat;
import ru.course.format.MetricsInputFormat;
import ru.course.util.ScaleUtil;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

@Log4j
public class Main {
    /**
     * @param args должны прийти три аргумента:
     *             1) имя обрабатываемой папки
     *             2) имя выходной папки
     */
    public static void main(String[] args) throws IOException, InterruptedException, ClassNotFoundException {
        if (args.length < 2) {
            throw new RuntimeException("Необходимо задать входной и выходной файл");
        }

        Configuration conf = loadConfig();

        Job job = Job.getInstance(conf, "avg metric");
        job.setJarByClass(Main.class);
        job.setMapOutputKeyClass(MetricsKeyOutWritable.class);
        job.setMapOutputValueClass(IntWritable.class);
        job.setOutputKeyClass(MetricsKeyOutWritable.class);
        job.setOutputValueClass(IntWritable.class);
        job.setMapperClass(MetricsMapper.class);
        job.setReducerClass(MetricsReduce.class);

        job.setInputFormatClass(MetricsInputFormat.class);
        job.setOutputFormatClass(MetricsCSVOutputFormat.class);

        FileInputFormat.addInputPath(job, new Path(args[1]));
        FileOutputFormat.setOutputPath(job, new Path(args[2]));
        job.setSpeculativeExecution(false);

        log.info("=====================JOB STARTED=====================");
        job.waitForCompletion(true);
        log.info("=====================JOB ENDED=====================");
    }

    private static Configuration loadConfig() throws IOException {
        Configuration conf = new Configuration();

        ClassLoader loader = Thread.currentThread().getContextClassLoader();
        InputStream stream = loader.getResourceAsStream("application.properties");
        Properties properties = new Properties();
        properties.load(stream);

        conf.set("metrics.scale", String.valueOf(
                ScaleUtil.getInstance(
                        properties.getProperty("metrics.scale"))));
        conf.set("metrics.scale.string", properties.getProperty("metrics.scale"));

        // задаём выходной файл, разделенный запятыми - формат CSV в соответствии с заданием
        conf.set("mapreduce.output.textoutputformat.separator", ",");

        return conf;
    }
}
