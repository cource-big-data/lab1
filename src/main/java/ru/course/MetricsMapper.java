package ru.course;

import lombok.NoArgsConstructor;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;
import ru.course.data.MetricsDataInWritable;
import ru.course.data.MetricsKeyOutWritable;

import java.io.IOException;

@NoArgsConstructor
public class MetricsMapper extends Mapper<IntWritable, MetricsDataInWritable, MetricsKeyOutWritable, IntWritable> {

    private long scale;
    private String scaleString;
    private final String[] valuesKeys = {"Node1CPU", "Node2CPU", "Node3CPU", "Node4CPU", "Node12HDD", "Node34HDD", "Node1Cache", "Node2Cache", "Node3Cache", "Node4Cache"};

    @Override
    protected void setup(Mapper<IntWritable, MetricsDataInWritable, MetricsKeyOutWritable, IntWritable>.Context context) throws IOException, InterruptedException {
        super.setup(context);
        Configuration conf = context.getConfiguration();
        scaleString = conf.get("metrics.scale.string");
        scale = conf.getLong("metrics.scale", 0);
    }

    @Override
    protected void map(IntWritable key, MetricsDataInWritable value, Mapper<IntWritable, MetricsDataInWritable, MetricsKeyOutWritable, IntWritable>.Context context) throws IOException, InterruptedException {

        long startTime = value.getTimeStamp().get() - value.getTimeStamp().get() % this.scale;
        int metricId = key.get() - 1;

        MetricsKeyOutWritable metricsKeyOutWritable = new MetricsKeyOutWritable(
                new Text(metricId < valuesKeys.length && metricId >= 0 ? valuesKeys[metricId] : "other"),
                new LongWritable(startTime),
                new Text(scaleString));

        context.write(metricsKeyOutWritable, value.getValue());
    }
}
