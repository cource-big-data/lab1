package ru.course.util;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.experimental.UtilityClass;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

@UtilityClass
public class ScaleUtil {
    public Long getInstance(String scale){
        Pattern pattern = Pattern.compile("^(\\d+)([smhd])$");
        Matcher matcher = pattern.matcher(scale);
        if (matcher.matches()) {
            return Long.parseLong(matcher.group(1)) * getMultiplier(matcher.group(2));
        } else {
            throw new IllegalStateException("No match found: " + scale);
        }
    }

    private Long getMultiplier(String type) {
        switch (type) {
            case "s": return ScaleEnum.SECOND.getMultiplier();
            case "m": return ScaleEnum.MINUTE.getMultiplier();
            case "h": return ScaleEnum.HOUR.getMultiplier();
            case "d": return ScaleEnum.DAY.getMultiplier();
            default: throw  new IllegalStateException("Unexpected value: " + type);
        }
    }

    @AllArgsConstructor
    @Getter
    public enum ScaleEnum {
        SECOND(1000),
        MINUTE(60000),
        HOUR(60*60000),
        DAY(24*60*60000);

        private final long multiplier;
    }
}
