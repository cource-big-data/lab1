package ru.course;

import lombok.NoArgsConstructor;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.mapreduce.Reducer;
import ru.course.data.MetricsKeyOutWritable;

import java.io.IOException;

@NoArgsConstructor
public class MetricsReduce extends Reducer<MetricsKeyOutWritable, IntWritable, MetricsKeyOutWritable, IntWritable> {

    @Override
    protected void reduce(MetricsKeyOutWritable key, Iterable<IntWritable> values, Context context) throws IOException, InterruptedException {
        int sum = 0;
        int count = 0;
        while (values.iterator().hasNext()) {
            sum += values.iterator().next().get();
            count++;
        }

        context.write(key, new IntWritable(sum/count));
    }
}
