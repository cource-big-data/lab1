package ru.course.format;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FSDataOutputStream;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.mapreduce.RecordWriter;
import org.apache.hadoop.mapreduce.TaskAttemptContext;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import ru.course.data.MetricsKeyOutWritable;

import java.io.IOException;

public class MetricsCSVOutputFormat<K, V> extends FileOutputFormat<K, V> {
    // File extension for created output files
    private static final String FILE_EXTENSION = ".csv";
    public static String FIELD_SEPARATOR = "mapreduce.output.textoutputformat.separator";
    public static String RECORD_SEPARATOR = "mapreduce.output.textoutputformat.recordseparator";
    @Override
    public RecordWriter<K, V> getRecordWriter(TaskAttemptContext job) throws IOException, InterruptedException {
        Configuration conf = job.getConfiguration();
        String fieldSeprator = conf.get(FIELD_SEPARATOR, ",");
        //custom record separator, \n used as a default
        String recordSeprator = conf.get(RECORD_SEPARATOR, "\n");
        Path file = this.getDefaultWorkFile(job, FILE_EXTENSION);
        FileSystem fs = file.getFileSystem(conf);
        FSDataOutputStream fileOut = fs.create(file, false);
        return new MetricsCSVRecordWriter<>(fileOut, fieldSeprator, recordSeprator);
    }
}
