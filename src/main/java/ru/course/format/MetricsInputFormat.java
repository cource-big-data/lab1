package ru.course.format;

import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.mapreduce.InputSplit;
import org.apache.hadoop.mapreduce.RecordReader;
import org.apache.hadoop.mapreduce.TaskAttemptContext;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import ru.course.data.MetricsDataInWritable;

import java.io.IOException;

public class MetricsInputFormat extends FileInputFormat<IntWritable, MetricsDataInWritable> {
    /**
     * Create a record reader for a given split. The framework will call
     * {@link RecordReader#initialize(InputSplit, TaskAttemptContext)} before
     * the split is used.
     *
     * @param split   the split to be read
     * @param context the information about the task
     * @return a new record reader
     * @throws IOException
     * @throws InterruptedException
     */
    @Override
    public RecordReader<IntWritable, MetricsDataInWritable> createRecordReader(InputSplit split, TaskAttemptContext context) throws IOException, InterruptedException {
        MetricsRecordReader metricsRecordReader = new MetricsRecordReader();
        metricsRecordReader.initialize(split, context);
        return metricsRecordReader;
    }
}
