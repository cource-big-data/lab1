package ru.course.data;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.io.WritableComparable;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class MetricsKeyOutWritable implements WritableComparable<MetricsKeyOutWritable> {

    private Text key = new Text();
    private LongWritable timeStamp = new LongWritable();
    private Text scale = new Text();

    @Override
    public void write(DataOutput dataOutput) throws IOException {
        this.key.write(dataOutput);
        this.timeStamp.write(dataOutput);
        this.scale.write(dataOutput);
    }

    @Override
    public void readFields(DataInput dataInput) throws IOException {
        this.key.readFields(dataInput);
        this.timeStamp.readFields(dataInput);
        this.scale.readFields(dataInput);
    }

    @Override
    public int compareTo(MetricsKeyOutWritable o) {

        if (this.key.compareTo(o.key) == 0 && this.timeStamp.compareTo(o.timeStamp) == 0 && this.scale.compareTo(o.scale) == 0) {
            return 0;
        } else {
            return 1;
        }

    }

    public String toString() {
        return this.getKey() + "," + this.getTimeStamp() + "," + this.getScale();
    }
}
