package ru.course.data;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Writable;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;

/**
 * @author Илья Каменских
 * @version 1.0
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class MetricsDataInWritable implements Writable {

    private LongWritable timeStamp = new LongWritable();
    private IntWritable value = new IntWritable();

    @Override
    public void write(DataOutput dataOutput) throws IOException {
        this.timeStamp.write(dataOutput);
        this.value.write(dataOutput);
    }

    @Override
    public void readFields(DataInput dataInput) throws IOException {
        this.timeStamp.readFields(dataInput);
        this.value.readFields(dataInput);
    }
}
