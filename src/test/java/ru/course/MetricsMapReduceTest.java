package ru.course;

import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mrunit.mapreduce.MapReduceDriver;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import ru.course.data.MetricsDataInWritable;
import ru.course.data.MetricsKeyOutWritable;
import ru.course.util.ScaleUtil;

import java.io.IOException;

public class MetricsMapReduceTest {

    private MapReduceDriver<IntWritable, MetricsDataInWritable, MetricsKeyOutWritable, IntWritable, MetricsKeyOutWritable, IntWritable> mapReduceDriver;

    @BeforeEach
    void setUp() {
        MetricsMapper mapper = new MetricsMapper();
        MetricsReduce reducer = new MetricsReduce();
        mapReduceDriver = MapReduceDriver.newMapReduceDriver(mapper, reducer);
        mapReduceDriver.getConfiguration().set("metrics.scale.string", "1m");
        mapReduceDriver.getConfiguration().set("metrics.scale", String.valueOf(ScaleUtil.getInstance("1m")));
    }

    @Test
    public void testMapReduce() throws IOException {
        mapReduceDriver
                .withInput(
                        new IntWritable(1),
                        new MetricsDataInWritable(new LongWritable(1510670916247l), new IntWritable(10)))
                .withInput(
                        new IntWritable(1),
                        new MetricsDataInWritable(new LongWritable(1510670916249l), new IntWritable(50)))
                .withOutput(
                        new MetricsKeyOutWritable(new Text("Node1CPU"), new LongWritable(1510670880000l), new Text("1m")),
                        new IntWritable(30))
                .runTest();
    }

}
