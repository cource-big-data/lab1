package ru.course;

import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mrunit.mapreduce.MapDriver;
import org.apache.hadoop.mrunit.mapreduce.ReduceDriver;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import ru.course.data.MetricsKeyOutWritable;
import ru.course.util.ScaleUtil;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class MetricsReduceTest {

    private ReduceDriver<MetricsKeyOutWritable, IntWritable, MetricsKeyOutWritable, IntWritable> reduceDriver;

    @BeforeEach
    void setUp() {
        MetricsReduce reducer = new MetricsReduce();
        reduceDriver = ReduceDriver.newReduceDriver(reducer);
        reduceDriver.getConfiguration().set("metrics.scale.string", "1m");
        reduceDriver.getConfiguration().set("metrics.scale", String.valueOf(ScaleUtil.getInstance("1m")));
    }

    @Test
    void reduce() throws IOException {
        List<IntWritable> values = new ArrayList<IntWritable>();
        values.add(new IntWritable(40));
        values.add(new IntWritable(60));
        reduceDriver
                .withInput(new MetricsKeyOutWritable(new Text("Node1CPU"), new LongWritable(1510670880000l), new Text("1m")), values)
                .withOutput(new MetricsKeyOutWritable(new Text("Node1CPU"), new LongWritable(1510670880000l), new Text("1m")), new IntWritable(50))
                .runTest();
    }
}