package ru.course;

import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import ru.course.data.MetricsDataInWritable;
import ru.course.data.MetricsKeyOutWritable;
import org.apache.hadoop.mrunit.mapreduce.MapDriver;
import ru.course.util.ScaleUtil;

import java.io.IOException;

class MetricsMapperTest {

    private MapDriver<IntWritable, MetricsDataInWritable, MetricsKeyOutWritable, IntWritable> mapDriver;

    @BeforeEach
    void setUp() {
        MetricsMapper mapper = new MetricsMapper();
        mapDriver = MapDriver.newMapDriver(mapper);
        mapDriver.getConfiguration().set("metrics.scale.string", "1m");
        mapDriver.getConfiguration().set("metrics.scale", String.valueOf(ScaleUtil.getInstance("1m")));
    }

    @Test
    void map() throws IOException {
        mapDriver
                .withInput(new IntWritable(1), new MetricsDataInWritable(new LongWritable(1510670916247l), new IntWritable(10)))
                .withInput(new IntWritable(1), new MetricsDataInWritable(new LongWritable(1510670916249l), new IntWritable(50)))
                .withOutput(new MetricsKeyOutWritable(new Text("Node1CPU"), new LongWritable(1510670880000l), new Text("1m")), new IntWritable(10))
                .withOutput(new MetricsKeyOutWritable(new Text("Node1CPU"), new LongWritable(1510670880000l), new Text("1m")), new IntWritable(50))
                .runTest();

    }
}