docker build -t hadoop-metrics ../
docker run --rm --network labs1_default --env-file ../hadoop.env hadoop-base:latest hdfs dfs -copyFromLocal -f /input /
docker run --rm --network labs1_default --env-file ../hadoop.env hadoop-metrics
docker run --rm --network labs1_default --env-file ../hadoop.env hadoop-base:latest hdfs dfs -cat /output/*
docker run --rm --network labs1_default --env-file ../hadoop.env hadoop-base:latest hdfs dfs -rm -r /output
docker run --rm --network labs1_default --env-file ../hadoop.env hadoop-base:latest hdfs dfs -rm -r /input
