docker build -t hadoop-base:latest ../docker/base
docker build -t hadoop-namenode:latest ../docker/namenode
docker build -t hadoop-datanode:latest ../docker/datanode
docker build -t hadoop-resourcemanager:latest ../docker/resourcemanager
docker build -t hadoop-nodemanager:latest ../docker/nodemanager
docker build -t hadoop-historyserver:latest ../docker/historyserver
docker-compose up -d
